package com.example.listview;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    public static List<Contacte> data = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button add = findViewById(R.id.btn_add);
        add.setOnClickListener(this);
    }

    @Override
    protected void onStart() {

        super.onStart();
        Log.i("wnicw", "bb2222222bb");
        update_data();
    }

    public void update_data() {
        fill_list();
        loadList();
        ((ListView) findViewById(R.id.main_list)).setOnItemClickListener((adapterView, view, i, l) -> {
            Intent intent = new Intent(getApplicationContext(), EditContacte.class);
            Bundle bundle = new Bundle();
            bundle.putInt("id", data.get(i).getId());
            intent.putExtras(bundle);
            startActivity(intent);
        });
    }

    public void fill_list() {
        DBContactes contactes = new DBContactes(this, "Contactes", null, 1);

        SQLiteDatabase db = contactes.getWritableDatabase();

        if (db == null) return;
        data.clear();

        String[] cols = new String[]{"id", "nom", "cognoms", "telefon"};

        Cursor cursor = db.query("Contactes", cols, null, null,
                null, null, null, null);


        if (!cursor.moveToFirst()) return;
        int id_col = cursor.getColumnIndex("id");
        int nom_col = cursor.getColumnIndex("nom");
        int cog_col = cursor.getColumnIndex("cognoms");
        int tel_col = cursor.getColumnIndex("telefon");
        do {
            String id = cursor.getString(id_col);
            String nom = cursor.getString(nom_col);
            String cog = cursor.getString(cog_col);
            String tel = cursor.getString(tel_col);
            data.add(new Contacte(Integer.parseInt(id), nom, cog, Integer.parseInt(tel)));
        } while (cursor.moveToNext());
        cursor.close();
        db.close();
    }

    void loadList() {
        AdaptadorContactes adapter = new AdaptadorContactes(this);
        ListView lv = findViewById(R.id.main_list);
        lv.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {
        int btn_id = view.getId();
        if (btn_id == R.id.btn_add) {
            Intent intent = new Intent(getApplicationContext(), EditContacte.class);
            Bundle bundle = new Bundle();
            bundle.putInt("id", 0);
            intent.putExtras(bundle);
            startActivity(intent);
            Log.i("wnicw", "bb2111111bb");

        }
    }

    static class AdaptadorContactes extends ArrayAdapter {
        Activity context;

        public AdaptadorContactes(Activity context) {
            super(context, R.layout.activity_main, data);
            this.context = context;
        }

        @NonNull
        @SuppressLint({"ViewHolder", "InflateParams", "SetTextI18n"})
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            LayoutInflater inflater = context.getLayoutInflater();
            View item = inflater.inflate(R.layout.item_view, null);
            TextView list_nom = item.findViewById(R.id.txt_nom);
            list_nom.setText(data.get(position).getNom());
            TextView list_cognoms = item.findViewById(R.id.txt_cognoms);
            list_cognoms.setText(data.get(position).getCognoms());
            TextView list_telefon = item.findViewById(R.id.txt_telefon);
            list_telefon.setText(data.get(position).getTelefon() + "");
            return (item);
        }
    }
}

