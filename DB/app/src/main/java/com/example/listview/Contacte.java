package com.example.listview;

import androidx.annotation.NonNull;

public class Contacte {

    private final int id;
    private final String nom;
    private final String cognoms;
    private final int telefon;

    public Contacte(int id, String nom, String cognoms, int telefon) {
        this.id = id;
        this.nom = nom;
        this.cognoms = cognoms;
        this.telefon = telefon;
    }

    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public String getCognoms() {
        return cognoms;
    }

    public int getTelefon() {
        return telefon;
    }

    @NonNull
    @Override
    public String toString() {
        return "Contacte{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", cognoms='" + cognoms + '\'' +
                ", telefon=" + telefon +
                '}';
    }
}
