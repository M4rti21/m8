package com.example.listview;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DBContactes extends SQLiteOpenHelper {

    String sqlCreation =
            "CREATE TABLE Contactes ( " +
                    "id integer primary key autoincrement, " +
                    "nom text  not null, " +
                    "cognoms text not null," +
                    "telefon integer not null)";

    public DBContactes(Context context, String name, CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sqlCreation);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS Contactes");
        db.execSQL(sqlCreation);
    }

}
