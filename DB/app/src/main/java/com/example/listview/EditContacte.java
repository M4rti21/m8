package com.example.listview;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class EditContacte extends AppCompatActivity implements View.OnClickListener {

    int id = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_contacte);

        Button accept = findViewById(R.id.btn_accept);
        accept.setOnClickListener(this);
        Button cancel = findViewById(R.id.btn_cancel);
        cancel.setOnClickListener(this);
        Button delete = findViewById(R.id.btn_del);
        delete.setOnClickListener(this);

        Bundle b = this.getIntent().getExtras();
        assert b != null;
        id = b.getInt("id");

        if (id == 0) return;

        DBContactes contactes = new DBContactes(this, "Contactes", null, 1);
        SQLiteDatabase db = contactes.getWritableDatabase();

        if (db == null) return;

        String[] cols = new String[]{"id", "nom", "cognoms", "telefon"};
        String where = "id = ?";
        String[] args = new String[]{String.valueOf(id)};

        Cursor cursor = db.query("Contactes", cols, where, args,
                null, null, null, null);

        cursor.moveToFirst();

        int nom_col = cursor.getColumnIndex("nom");
        int cog_col = cursor.getColumnIndex("cognoms");
        int tel_col = cursor.getColumnIndex("telefon");

        String nom = cursor.getString(nom_col);
        String cog = cursor.getString(cog_col);
        String tel = cursor.getString(tel_col);

        cursor.close();
        db.close();

        EditText inp_nom = findViewById(R.id.inp_nom);
        EditText inp_cog = findViewById(R.id.inp_cog);
        EditText inp_tel = findViewById(R.id.inp_tel);

        inp_nom.setText(nom);
        inp_cog.setText(cog);
        inp_tel.setText(tel);
        // Do something with the cursor data
    }

    @Override
    public void onClick(View view) {
        int btn_id = view.getId();
        if (btn_id == R.id.btn_cancel) {
            finish();
            return;
        }
        DBContactes contactes = new DBContactes(this, "Contactes", null, 1);
        SQLiteDatabase db = contactes.getWritableDatabase();
        if (db == null) return;

        if (btn_id == R.id.btn_del) {
            String where = "id = ?";
            String[] args = new String[]{String.valueOf(id)};
            db.delete("Contactes", where, args);
            db.close();
            finish();
            return;
        }

        if (btn_id == R.id.btn_accept) {
            String nom = ((EditText) findViewById(R.id.inp_nom)).getText().toString().trim();
            String cog = ((EditText) findViewById(R.id.inp_cog)).getText().toString().trim();
            int tel = Integer.parseInt(((EditText) findViewById(R.id.inp_tel)).getText().toString().trim());
            Contacte c = new Contacte(0, nom, cog, tel);
            ContentValues cv = create_register(c);
            if (id == 0) {
                db.insert("Contactes", null, cv);
            } else {
                String where = "id = ?";
                String[] args = new String[]{String.valueOf(id)};
                db.update("Contactes", cv, where, args);
            }
            db.close();
            finish();
        }
    }

    private ContentValues create_register(Contacte c) {
        ContentValues r = new ContentValues();
        r.put("nom", c.getNom());
        r.put("cognoms", c.getCognoms());
        r.put("telefon", c.getTelefon());
        return r;
    }
}