package com.example.proiecte;

import static com.example.proiecte.MainActivity.noSQL;
import static com.example.proiecte.MainActivity.throwToast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class Game extends AppCompatActivity {

    private boolean win = false;
    private Button[][] buttons;
    private boolean turn = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        Button btn_end_game = findViewById(R.id.btn_end_game);
        btn_end_game.setOnClickListener(v -> endGame());

        ImageView win_image = findViewById(R.id.win_image);
        win_image.setImageResource(0);
        TextView win_text = findViewById(R.id.win_text);
        win_text.setText("");

        getTable();
    }

    private void getTable() {
        buttons = new Button[3][3];
        buttons[0][0] = findViewById(R.id.btn_00);
        buttons[0][1] = findViewById(R.id.btn_01);
        buttons[0][2] = findViewById(R.id.btn_02);
        buttons[1][0] = findViewById(R.id.btn_10);
        buttons[1][1] = findViewById(R.id.btn_11);
        buttons[1][2] = findViewById(R.id.btn_12);
        buttons[2][0] = findViewById(R.id.btn_20);
        buttons[2][1] = findViewById(R.id.btn_21);
        buttons[2][2] = findViewById(R.id.btn_22);
        buttons[0][0].setOnClickListener(v -> setClicked(buttons[0][0]));
        buttons[0][1].setOnClickListener(v -> setClicked(buttons[0][1]));
        buttons[0][2].setOnClickListener(v -> setClicked(buttons[0][2]));
        buttons[1][0].setOnClickListener(v -> setClicked(buttons[1][0]));
        buttons[1][1].setOnClickListener(v -> setClicked(buttons[1][1]));
        buttons[1][2].setOnClickListener(v -> setClicked(buttons[1][2]));
        buttons[2][0].setOnClickListener(v -> setClicked(buttons[2][0]));
        buttons[2][1].setOnClickListener(v -> setClicked(buttons[2][1]));
        buttons[2][2].setOnClickListener(v -> setClicked(buttons[2][2]));
    }

    private void endGame() {
        for (Button[] button : buttons) {
            for (Button value : button) {
                value.setEnabled(false);
            }
        }
        if (win) {
            Database db = new Database(this, null, MainActivity.DB_VERSION);
            SQLiteDatabase sql = db.getWritableDatabase();
            if (noSQL(this, sql)) return;

            int id = getSharedPreferences("user", MODE_PRIVATE).getInt("id", 0);
            if (id == 0) {
                throwToast(this, "couldn't find user id");
                sql.close();
                finish();
                return;
            }
            int win_score = 10;
            sql.execSQL("UPDATE Users SET score = score + " + win_score + " WHERE id = " + id);
            sql.close();
        }
        finish();
    }

    private void setClicked(Button button) {
        if (!turn) return;
        button.setText("X");
        button.setEnabled(false);
        button.setBackgroundColor(0xFF00FF00);
        button.setTextColor(0xFF000000);
        turn = false;
        if (checkWin()) {
            setWinTitle();
            return;
        }
        cpuClick();
    }

    private void cpuClick() {
        turn = false;
        boolean full = true;
        for (Button[] button : buttons) {
            for (Button value : button) {
                if (value.isEnabled()) {
                    full = false;
                    break;
                }
            }
        }
        if (full) {
            throwToast(this, "DRAW");
            finish();
            return;
        }

        int i = (int) (Math.random() * 3);
        int j = (int) (Math.random() * 3);
        while (!buttons[i][j].isEnabled()) {
            i = (int) (Math.random() * 3);
            j = (int) (Math.random() * 3);
        }
        buttons[i][j].setText("O");
        buttons[i][j].setEnabled(false);
        buttons[i][j].setBackgroundColor(0xFFFF0000);
        buttons[i][j].setTextColor(0xFF000000);
        if (checkWin()) {
            setWinTitle();
            return;
        }
        turn = true;
    }

    private void setWinTitle() {
        ImageView win_image = findViewById(R.id.win_image);
        TextView win_text = findViewById(R.id.win_text);
        win_image.setEnabled(true);
        win_text.setEnabled(true);
        if (win) {
            win_image.setImageResource(R.drawable.tick);
            win_text.setText(R.string.won);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "channel_id")
                    .setSmallIcon(R.drawable.tick)
                    .setContentTitle(getString(R.string.won))
                    .setContentText(getString(R.string.won))
                    .setAutoCancel(true);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel channel = new NotificationChannel("channel_id", "Channel Name", NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
            notificationManager.notify(1, builder.build());
            MediaPlayer mediaPlayer = MediaPlayer.create(this, R.raw.win);
            mediaPlayer.start();
        } else {
            win_image.setImageResource(R.drawable.fail);
            win_text.setText(R.string.lost);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "channel_id")
                    .setSmallIcon(R.drawable.fail)
                    .setContentTitle(getString(R.string.lost))
                    .setContentText(getString(R.string.lost))
                    .setAutoCancel(true);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel channel = new NotificationChannel("channel_id", "Channel Name", NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
            notificationManager.notify(1, builder.build());
            MediaPlayer mediaPlayer = MediaPlayer.create(this, R.raw.loose);
            mediaPlayer.start();
        }
    }

    private boolean checkWin() {
        for (int i = 0; i < 3; i++) {
            if (buttons[i][0].getText().equals(buttons[i][1].getText()) &&
                    buttons[i][1].getText().equals(buttons[i][2].getText()) &&
                    !buttons[i][0].isEnabled()) {
                if (buttons[i][0].getText().equals("X")) {
                    throwToast(this, "YOU WON!!");
                    win = true;
                } else {
                    throwToast(this, "YOU LOST :(");
                }
                return true;
            }
        }
        for (int i = 0; i < 3; i++) {
            if (buttons[0][i].getText().equals(buttons[1][i].getText()) &&
                    buttons[1][i].getText().equals(buttons[2][i].getText()) &&
                    !buttons[0][i].isEnabled()) {
                if (buttons[0][i].getText().equals("X")) {
                    throwToast(this, "YOU WON!!");
                    win = true;
                } else {
                    throwToast(this, "YOU LOST :(");
                }
                return true;
            }
        }
        if (buttons[0][0].getText().equals(buttons[1][1].getText()) &&
                buttons[1][1].getText().equals(buttons[2][2].getText()) &&
                !buttons[0][0].isEnabled()) {
            if (buttons[0][0].getText().equals("X")) {
                throwToast(this, "YOU WON!!");
                win = true;
            } else {
                throwToast(this, "YOU LOST :(");
            }
            return true;
        }
        if (buttons[0][2].getText().equals(buttons[1][1].getText()) &&
                buttons[1][1].getText().equals(buttons[2][0].getText()) &&
                !buttons[0][2].isEnabled()) {
            if (buttons[0][2].getText().equals("X")) {
                throwToast(this, "YOU WON!!");
                win = true;
            } else {
                throwToast(this, "YOU LOST :(");
            }
            return true;
        }
        win = false;
        return false;
    }
}