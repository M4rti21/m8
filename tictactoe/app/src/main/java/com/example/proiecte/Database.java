package com.example.proiecte;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Database extends SQLiteOpenHelper {
    String table_genders =
            "CREATE TABLE IF NOT EXISTS Genders ( " +
                    "gender_id integer primary key autoincrement, " +
                    "name text not null)";

    String table_users =
            "CREATE TABLE IF NOT EXISTS Users ( " +
                    "id integer primary key autoincrement, " +
                    "username text  not null, " +
                    "email text not null," +
                    "password text not null," +
                    "gender_id integer not null," +
                    "score integer not null default 0," +
                    "FOREIGN KEY (gender_id) REFERENCES Genders(gender_id))";

    private void createTables(SQLiteDatabase db) {
        db.execSQL(table_genders);
        db.execSQL("INSERT INTO Genders (name) VALUES ('male')");
        db.execSQL("INSERT INTO Genders (name) VALUES ('female')");
        db.execSQL("INSERT INTO Genders (name) VALUES ('other')");
        db.execSQL(table_users);
    }

    public Database(Context context, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, "ticktacktoe", factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createTables(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS Contactes");
        createTables(db);
    }

}
