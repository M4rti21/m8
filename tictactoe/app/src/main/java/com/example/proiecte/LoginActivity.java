package com.example.proiecte;

import static com.example.proiecte.MainActivity.noSQL;
import static com.example.proiecte.MainActivity.throwToast;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button cancel = findViewById(R.id.btn_signup_cancel);
        cancel.setOnClickListener(v -> finish());
        Button create = findViewById(R.id.btn_signup_create);
        create.setOnClickListener(v -> click_create());
    }

    public void click_create() {
        String email = ((TextView) findViewById(R.id.inp_email)).getText().toString();
        String password = ((TextView) findViewById(R.id.inp_password)).getText().toString();

        Database db = new Database(this, null, MainActivity.DB_VERSION);
        SQLiteDatabase sql = db.getWritableDatabase();

        if (noSQL(this, sql)) return;

        String[] cols = new String[]{"id"};
        String where = "email = ? AND password = ?";
        String[] args = new String[]{email, password};

        Cursor c = sql.query("Users", cols, where, args, null, null, null, null);

        if (!c.moveToFirst()) {
            throwToast(this, "wrong email or password");
            sql.close();
            c.close();
            return;
        }
        int id = c.getInt(0);
        throwToast(this, "logged in as " + email);
        c.close();

        SharedPreferences sp = getSharedPreferences("user", MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt("id", id);
        editor.apply();

        Intent intent = new Intent(getApplicationContext(), Leaderboards.class);
        startActivity(intent);
    }
}