package com.example.proiecte;

import static com.example.proiecte.MainActivity.noSQL;
import static com.example.proiecte.MainActivity.throwToast;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class Leaderboards extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leaderboards);

        Button exit = findViewById(R.id.btn_exit);
        exit.setOnClickListener(v -> finish());
        Button play = findViewById(R.id.btn_play);
        play.setOnClickListener(v -> clickPlay());
        fillLeaderboards();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        fillLeaderboards();
    }

    private void clickPlay() {
        Intent intent = new Intent(getApplicationContext(), Game.class);
        startActivity(intent);
    }


    private void fillLeaderboards() {
        Database db = new Database(this, null, MainActivity.DB_VERSION);
        SQLiteDatabase sql = db.getWritableDatabase();
        Cursor c = sql.rawQuery("SELECT username, score FROM Users ORDER BY score DESC;", null);
        if (!c.moveToFirst()) {
            sql.close();
            c.close();
            return;
        }
        List<String> users = new ArrayList<>();
        do {
            users.add(c.getString(0) + " -> " + c.getString(1));
        } while (c.moveToNext());
        c.close();
        sql.close();
        System.out.println(users);
        ListView lv = findViewById(R.id.list_leaderboards);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this,
                android.R.layout.simple_list_item_1,
                users
        );
        lv.setAdapter(adapter);
    }
}