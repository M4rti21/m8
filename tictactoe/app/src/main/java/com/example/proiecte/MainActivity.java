package com.example.proiecte;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public static final int DB_VERSION = 5;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Database db = new Database(this, null, DB_VERSION);
        SQLiteDatabase sql = db.getWritableDatabase();
        sql.close();
        Button btn_login = findViewById(R.id.btn_login);
        btn_login.setOnClickListener(v -> clickLogin());
        Button btn_signup = findViewById(R.id.btn_signup);
        btn_signup.setOnClickListener(v -> clickSignup());
        //Button btn_test = findViewById(R.id.btn_test);
        //btn_test.setOnClickListener(v -> clickTest());
    }

    private void clickLogin() {
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
    }

    private void clickSignup() {
        Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
        startActivity(intent);
    }

    private void clickTest() {
        Intent intent = new Intent(getApplicationContext(), Game.class);
        startActivity(intent);
    }

    public static void throwToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static boolean noSQL(Context context, SQLiteDatabase sql) {
        if (sql == null) {
            throwToast(context, "couldn't connect to the database");
            return true;
        }
        return false;
    }
}