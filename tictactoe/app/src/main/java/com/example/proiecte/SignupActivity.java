package com.example.proiecte;

import static com.example.proiecte.MainActivity.noSQL;
import static com.example.proiecte.MainActivity.throwToast;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SignupActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        Database db = new Database(this, null, MainActivity.DB_VERSION);
        SQLiteDatabase sql = db.getWritableDatabase();

        if (noSQL(this, sql)) {
            finish();
            return;
        }

        Cursor c = sql.rawQuery("SELECT * FROM Genders", null);

        if (!c.moveToFirst()) {
            sql.close();
            c.close();
            return;
        }

        List<String> genders = new ArrayList<>();
        do {
            genders.add(c.getString(1));
        } while (c.moveToNext());
        sql.close();
        c.close();

        Spinner dropdown = findViewById(R.id.drp_gender);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this,
                android.R.layout.simple_spinner_dropdown_item,
                genders
        );
        dropdown.setAdapter(adapter);

        Button cancel = findViewById(R.id.btn_signup_cancel);
        cancel.setOnClickListener(this);
        Button create = findViewById(R.id.btn_signup_create);
        create.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int btn_id = view.getId();
        if (btn_id == R.id.btn_signup_cancel) {
            finish();
        }
        if (btn_id == R.id.btn_signup_create) {
            String username = ((TextView) findViewById(R.id.inp_username)).getText().toString();
            String email = ((TextView) findViewById(R.id.inp_email)).getText().toString();
            String password = ((TextView) findViewById(R.id.inp_password)).getText().toString();

            if (!validInputs(username, email, password)) return;

            insertUser(username, email, password);

            finish();
        }
    }

    private void insertUser(String username, String email, String password) {
        Database db = new Database(this, null, MainActivity.DB_VERSION);
        SQLiteDatabase sql = db.getWritableDatabase();

        if (noSQL(this, sql)) return;

        ContentValues cv = new ContentValues();
        cv.put("username", username);
        cv.put("email", email);
        cv.put("password", password);
        cv.put("gender_id", 1);

        sql.insert("Users", null, cv);
        sql.close();

        throwToast(this, "successfully created an account");
    }

    private boolean validInputs(String username, String email, String password) {

        boolean terms = ((CheckBox) findViewById(R.id.inp_terms)).isChecked();

        if (!validUsername(username)) return false;
        if (!validEmail(email)) return false;
        if (!validPassword(password)) return false;
        if (!validTerms(terms)) return false;

        Database db = new Database(this, null, MainActivity.DB_VERSION);
        SQLiteDatabase sql = db.getWritableDatabase();

        if (noSQL(this, sql)) return false;
        if (usernameExists(sql, username)) return false;
        if (emailExists(sql, email)) return false;

        sql.close();

        return true;
    }

    private boolean validUsername(String username) {
        if (username.length() < 3) {
            throwToast(this, "username is too short");
            return false;
        }
        return true;
    }

    private boolean validEmail(String email) {
        if (email.length() < 3) {
            throwToast(this, "email is too short");
            return false;
        }
        Pattern email_pattern = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = email_pattern.matcher(email);
        if (!matcher.matches()) {
            throwToast(this, "email format is not valid");
            return false;
        }
        return true;
    }

    private boolean validPassword(String password) {
        if (password.length() < 3) {
            throwToast(this, "password is too short");
            return false;
        }
        if (password.toLowerCase().equals(password)) {
            throwToast(this, "password must have at least one uppercase letter");
            return false;
        }
        if (password.toUpperCase().equals(password)) {
            throwToast(this, "password must have at least one lowercase letter");
            return false;
        }
        return true;
    }

    private boolean validTerms(boolean terms) {
        if (!terms) {
            throwToast(this, "terms and conditions need to be accepted");
            return false;
        }
        return true;
    }

    private boolean usernameExists(SQLiteDatabase sql, String username) {
        String[] cols = new String[]{"id"};
        String where = "username = ?";
        String[] args = new String[]{username};

        Cursor c = sql.query("Users", cols, where, args, null, null, null, null);

        if (c.moveToFirst()) {
            throwToast(this, "there is already a user with this username");
            sql.close();
            c.close();
            return true;
        }
        c.close();
        return false;
    }

    private boolean emailExists(SQLiteDatabase sql, String email) {
        String[] cols = new String[]{"id"};
        String where = "email = ?";
        String[] args = new String[]{email};

        Cursor c = sql.query("Users", cols, where, args, null, null, null, null);

        if (c.moveToFirst()) {
            throwToast(this, "there is already a user with this email");
            sql.close();
            c.close();
            return true;
        }

        c.close();
        return false;
    }

}