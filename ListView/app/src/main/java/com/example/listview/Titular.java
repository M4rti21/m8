package com.example.listview;

import android.util.Log;

public class Titular {
    private String titol;
    private String subtitol;

    public Titular(String tit, String sub) {
        titol = tit;
        subtitol = sub;
        Log.println(Log.INFO, "test", getTitol());
    }
    public String getTitol() {
        return titol;
    }

    public void setTitol(String titol) {
        this.titol = titol;
    }

    public String getSubtitol() {
        return subtitol;
    }

    public void setSubtitol(String subtitol) {
        this.subtitol = subtitol;
    }
}
