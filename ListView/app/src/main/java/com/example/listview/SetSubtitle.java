package com.example.listview;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class SetSubtitle extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.subtitle_view);
        Button accept = findViewById(R.id.btn_accept);
        accept.setOnClickListener(this);
        Button cancel = findViewById(R.id.btn_cancel);
        cancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        if (id == R.id.btn_cancel) {
            setResult(RESULT_CANCELED, i);
            finish();
        }
        String txt = ((EditText) findViewById(R.id.inp_subtitle)).getText().toString().trim();
        i.putExtra("subtitle", txt);
        setResult(RESULT_OK, i);
        finish();
    }
}