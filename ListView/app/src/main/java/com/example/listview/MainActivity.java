package com.example.listview;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    final Titular[] dadesE = new Titular[]{
            new Titular("Titulo 1", "Substitulo 1"),
            new Titular("Titol 2", "Substitol 2"),
            new Titular("Titol 3", "Substitol 3"),
            new Titular("Titol 4", "Substitol 4"),
            new Titular("Titol 5", "Substitol 5"),
            new Titular("Titol 6", "Substitol 6"),
            new Titular("Titol 7", "Substitol 7"),
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loadList();
        ((ListView) findViewById(R.id.main_list)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getApplicationContext(), SetSubtitle.class);
                Bundle bundle = new Bundle();
                bundle.putString("titol", dadesE[i].getTitol());
                bundle.putString("subtitol", dadesE[i].getSubtitol());
                intent.putExtras(bundle);
                startActivityForResult(intent, i);
            }
        });
    }

    void loadList() {
        AdaptadorTitulars adapter = new AdaptadorTitulars(this);
        ListView lv = (ListView) findViewById(R.id.main_list);
        lv.setAdapter(adapter);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            MainActivity.super.onActivityResult(requestCode, resultCode, data);
            dadesE[requestCode].setSubtitol(data.getStringExtra("subtitle"));
            loadList();
        }
    }

    class AdaptadorTitulars extends ArrayAdapter {
        Activity context;
        public AdaptadorTitulars(Activity context) {
            super(context, R.layout.activity_main, dadesE);
            this.context = (Activity) context;
        }

        @SuppressLint({"ViewHolder", "InflateParams"})
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = context.getLayoutInflater();
            View item = inflater.inflate(R.layout.item_view, null);
            TextView list_titol = (TextView) item.findViewById(R.id.txt_title1);
            list_titol.setText(dadesE[position].getTitol());
            TextView list_subtitol = (TextView) item.findViewById(R.id.txt_subtitle1);
            list_subtitol.setText(dadesE[position].getSubtitol());
            return (item);
        }
    }
}

