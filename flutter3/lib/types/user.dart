class User {
  final int globalRank;
  final double pp;
  final int rankedScore;
  final double hitAccuracy;
  final int playCount;
  final int playTime;
  final int totalScore;
  final int totalHits;
  final int maximumCombo;
  final GradeCounts gradeCounts;
  final UserInfo user;

  User({
    required this.globalRank,
    required this.pp,
    required this.rankedScore,
    required this.hitAccuracy,
    required this.playCount,
    required this.playTime,
    required this.totalScore,
    required this.totalHits,
    required this.maximumCombo,
    required this.gradeCounts,
    required this.user,
  });

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      globalRank: json['global_rank'] as int,
      pp: (json['pp'] as num).toDouble(),
      rankedScore: json['ranked_score'] as int,
      hitAccuracy: (json['hit_accuracy'] as num).toDouble(),
      playCount: json['play_count'] as int,
      playTime: json['play_time'] as int,
      totalScore: json['total_score'] as int,
      totalHits: json['total_hits'] as int,
      maximumCombo: json['maximum_combo'] as int,
      gradeCounts: GradeCounts.fromJson(json['grade_counts']),
      user: UserInfo.fromJson(json['user']),
    );
  }
}

class Level {
  final int current;

  final int progress;

  Level({
    required this.current,
    required this.progress,
  });

  factory Level.fromJson(Map<String, dynamic> json) {
    return Level(
      current: json['current'] as int,
      progress: json['progress'] as int,
    );
  }
}

class GradeCounts {
  final int ss;

  final int ssh;

  final int s;

  final int sh;

  final int a;

  GradeCounts({
    required this.ss,
    required this.ssh,
    required this.s,
    required this.sh,
    required this.a,
  });

  factory GradeCounts.fromJson(Map<String, dynamic> json) {
    return GradeCounts(
      ss: json['ss'] as int,
      ssh: json['ssh'] as int,
      s: json['s'] as int,
      sh: json['sh'] as int,
      a: json['a'] as int,
    );
  }
}

class UserInfo {
  final String avatarUrl;
  final bool isOnline;
  final String username;
  final CountryInfo country;
  final CoverInfo cover;
  final int id;

  UserInfo({
    required this.avatarUrl,
    required this.isOnline,
    required this.username,
    required this.country,
    required this.cover,
    required this.id,
  });

  factory UserInfo.fromJson(Map<String, dynamic> json) {
    return UserInfo(
      id: json['id'] as int,
      avatarUrl: json['avatar_url'] as String,
      isOnline: json['is_online'] as bool,
      username: json['username'] as String,
      country: CountryInfo.fromJson(json['country']),
      cover: CoverInfo.fromJson(json['cover']),
    );
  }
}

class CountryInfo {
  final String code;
  final String name;

  CountryInfo({
    required this.code,
    required this.name,
  });

  factory CountryInfo.fromJson(Map<String, dynamic> json) {
    return CountryInfo(
      code: json['code'] as String,
      name: json['name'] as String,
    );
  }
}

class CoverInfo {
  final String url;
  final dynamic id;

  CoverInfo({
    required this.url,
    required this.id,
  });

  factory CoverInfo.fromJson(Map<String, dynamic> json) {
    return CoverInfo(
      url: json['url'] as String,
      id: json['id'],
    );
  }
}
