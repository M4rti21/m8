import 'package:flutter/material.dart';
import 'package:flutter3/types/user.dart';
import 'package:flutter3/utils/colors.dart';
import 'package:flutter3/utils/http_service.dart';
import 'package:flutter3/utils/ip.dart';
import 'package:flutter3/widgets/search_widget.dart';
import 'package:flutter3/widgets/user_widget.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  HttpService http = HttpService(Ip.casa);
  List<User> users = [];

  Future<void> getApiUsers() async {
    List<User> res = await http.getUsers();
    setState(() {
      users = res;
    });
  }

  double padding = 16;

  @override
  void initState() {
    super.initState();
    getApiUsers();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.lightBackground,
      appBar: AppBar(
        backgroundColor: CustomColors.background,
        title: const Text(
          'osu!rankings',
          style: TextStyle(
            color: CustomColors.foreground,
          ),
        ),
        centerTitle: true,
        leading: const SearchWidget(),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              getApiUsers();
            },
            child: const Icon(
              Icons.refresh,
              color: CustomColors.foreground,
            ),
          ),
        ],
      ),
      body: Center(
        child: ListView.builder(
          itemCount: users.length,
          itemBuilder: (BuildContext context, int index) {
            return Padding(
              padding: EdgeInsets.symmetric(
                  vertical: padding / 2, horizontal: padding),
              child: UserWidget(user: users[index]),
            );
          },
        ),
      ),
    );
  }
}
