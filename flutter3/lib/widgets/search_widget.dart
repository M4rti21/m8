import 'package:flutter/material.dart';
import 'package:flutter3/utils/colors.dart';

class SearchWidget extends StatelessWidget {
  const SearchWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        showModalBottomSheet(
          context: context,
          isDismissible: true,
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              topRight: Radius.circular(20),
            ),
          ),
          builder: (context) {
            return Container(
              height: 600,
              color: CustomColors.background,
              child: const Center(
                child: Text('Search Widget'),
              ),
            );
          },
        );
      },
      child: const Icon(
        Icons.search,
        color: CustomColors.foreground,
      ),
    );
  }
}
