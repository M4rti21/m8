import 'package:flutter/material.dart';
import 'package:flutter3/types/user.dart';

class UserWidget extends StatelessWidget {
  final User user;

  const UserWidget({
    super.key,
    required this.user,
  });

  String truncate(String text, int length) {
    if (text.length > length) {
      return text.replaceRange(length, text.length, '...');
    }
    return text;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: NetworkImage(user.user.cover.url),
          fit: BoxFit.cover,
          // darken the image and blur it
          colorFilter:
              ColorFilter.mode(Colors.black.withOpacity(0.8), BlendMode.darken),
        ),
        borderRadius: BorderRadius.circular(12),
        boxShadow: const [
          BoxShadow(
            color: Colors.black38,
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3),
          ),
        ],
      ),
      child: Row(
        children: <Widget>[
          ClipRRect(
              borderRadius: BorderRadius.circular(12),
              child: Image.network(
                user.user.avatarUrl,
                width: 100,
                height: 100,
              )),
          Padding(
            padding:
                const EdgeInsets.only(left: 16, right: 16, top: 8, bottom: 8),
            child: Column(
              children: <Widget>[
                Row(children: <Widget>[
                  Image.network(
                    'https://flagcdn.com/h40/${user.user.country.code.toLowerCase()}.jpg',
                    width: 30,
                    height: 20,
                  ),
                  const SizedBox(width: 12),
                  Text(
                    truncate(user.user.username, 13),
                    style: const TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ]),
                Row(
                  children: <Widget>[
                    Text(
                      "#${user.globalRank}",
                      style: const TextStyle(
                        fontSize: 20,
                        color: Colors.white,
                      ),
                      textAlign: TextAlign.left,
                    ),
                    const SizedBox(width: 12),
                    Text(
                      "${user.pp.round()}pp",
                      style: const TextStyle(
                        fontSize: 20,
                        color: Colors.white,
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
