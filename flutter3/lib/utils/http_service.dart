import 'dart:convert';
import 'package:flutter3/types/post.dart';
import 'package:flutter3/types/user.dart';
import 'package:flutter3/utils/ip.dart';
import 'package:http/http.dart';

class HttpService {
  String ip = "";

  HttpService(Ip location) {
    switch (location) {
      case Ip.casa:
        ip = "192.168.122.1";
        break;
      case Ip.cole:
        ip = "172.18.250.2";
        break;
      default:
        ip = "127.0.0.1";
        break;
    }
  }

  Future<List<Post>> getPosts() async {
    Response res =
        await get(Uri.parse("https://jsonplaceholder.typicode.com/posts"));
    if (res.statusCode == 200) {
      List<dynamic> body = jsonDecode(res.body);
      List<Post> posts =
          body.map((dynamic item) => Post.fromJson(item)).toList();
      return posts;
    } else {
      throw "Unable to retrieve posts.";
    }
  }

  Future<List<User>> getUsers() async {
    Response res = await get(Uri.parse("http://$ip:4000/json/rankings"));
    if (res.statusCode == 200) {
      List<dynamic> body = jsonDecode(res.body);
      List<User> users =
          body.map((dynamic item) => User.fromJson(item)).toList();
      return users;
    } else {
      throw "Unable to retrieve posts.";
    }
  }
}
