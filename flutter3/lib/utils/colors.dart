import 'package:flutter/material.dart';

class CustomColors {
  static const Color foreground = Color(0xfff7f8f2);
  static const Color background = Color(0xff282a36);
  static const Color lightBackground = Color(0xff414558);
}
