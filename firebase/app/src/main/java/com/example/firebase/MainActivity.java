package com.example.firebase;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FirebaseApp.initializeApp(this);
        FirebaseDatabase db = FirebaseDatabase.getInstance();
        DatabaseReference myRef = db.getReference("message");
        myRef.setValue("Hola, soc en Marti");

        // create 5 example users

        User user1 = new User("Marti", "marti@gmail.com", "123456", "123456789");
        User user2 = new User("Pau", "pau@gmail.com", "123456", "123456789");
        User user3 = new User("Joan", "joan@gmail.com", "123456", "123456789");
        User user4 = new User("Oriol", "oriol@gmail.com", "123456", "123456789");
        User user5 = new User("Pere", "pere@gmail.com", "123456", "123456789");

        // write the users to the firebase app

        DatabaseReference usersRef = db.getReference("users");
        usersRef.child("1").setValue(user1);
        usersRef.child("2").setValue(user2);
        usersRef.child("3").setValue(user3);
        usersRef.child("4").setValue(user4);
        usersRef.child("5").setValue(user5);

    }
}