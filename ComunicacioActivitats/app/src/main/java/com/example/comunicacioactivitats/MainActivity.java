package com.example.comunicacioactivitats;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button validate = findViewById(R.id.btn_validate);
        validate.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_validate) {
            EditText name = findViewById(R.id.inp_name);
            Intent i = new Intent(getApplicationContext(), MainActivity2.class);
            Bundle b = new Bundle();
            b.putString("txt", name.getText().toString());
            i.putExtras(b);
            startActivityForResult(i, 1);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        TextView t = findViewById(R.id.txt_restult);
        assert data != null;
        String a = "Has " + data.getStringExtra("txt");
        t.setText(a);
    }
}