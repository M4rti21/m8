package com.example.comunicacioactivitats;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity2 extends AppCompatActivity implements View.OnClickListener  {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Bundle b = this.getIntent().getExtras();
        String txt = "Hola " + b.getString("txt");
        TextView t = findViewById(R.id.txt_show);
        t.setText(txt);
        Button b1 = findViewById(R.id.btn_a);
        b1.setOnClickListener(this);
        Button b2 = findViewById(R.id.btn_c);
        b2.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_a) {
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            i.putExtra("txt", "accept");
            setResult(RESULT_OK, i);
            finish();
        } else if (view.getId() == R.id.btn_c) {
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            i.putExtra("txt", "decline");
            setResult(RESULT_OK, i);
            finish();
        }
    }
}