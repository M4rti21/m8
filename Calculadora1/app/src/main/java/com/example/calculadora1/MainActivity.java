package com.example.calculadora1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    static boolean nou = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        List<Button> btn_s = new ArrayList<>();
        btn_s.add(findViewById(R.id.btn_0));
        btn_s.add(findViewById(R.id.btn_1));
        btn_s.add(findViewById(R.id.btn_2));
        btn_s.add(findViewById(R.id.btn_3));
        btn_s.add(findViewById(R.id.btn_4));
        btn_s.add(findViewById(R.id.btn_5));
        btn_s.add(findViewById(R.id.btn_6));
        btn_s.add(findViewById(R.id.btn_7));
        btn_s.add(findViewById(R.id.btn_8));
        btn_s.add(findViewById(R.id.btn_9));

        btn_s.add(findViewById(R.id.btn_clear));
        btn_s.add(findViewById(R.id.btn_equals));
        btn_s.add(findViewById(R.id.btn_divide));
        btn_s.add(findViewById(R.id.btn_multiply));
        btn_s.add(findViewById(R.id.btn_plus));
        btn_s.add(findViewById(R.id.btn_minus));

        for (Button btn : btn_s) {
            btn.setOnClickListener(this);
        }

    }

    @Override
    public void onClick(View v) {
        TextView txt = findViewById(R.id.txt_main);
        String str = (String) txt.getText();
        if (nou) {
            nou = false;
            str = "";
        }
        int id = v.getId();
        if (id == R.id.btn_0) str += "0";
        if (id == R.id.btn_1) str += "1";
        if (id == R.id.btn_2) str += "2";
        if (id == R.id.btn_3) str += "3";
        if (id == R.id.btn_4) str += "4";
        if (id == R.id.btn_5) str += "5";
        if (id == R.id.btn_6) str += "6";
        if (id == R.id.btn_7) str += "7";
        if (id == R.id.btn_8) str += "8";
        if (id == R.id.btn_9) str += "9";
        if (id == R.id.btn_divide) str += "/";
        if (id == R.id.btn_multiply) str += "*";
        if (id == R.id.btn_plus) str += "+";
        if (id == R.id.btn_minus) str += "-";
        if (id == R.id.btn_clear) str = "";
        if (id == R.id.btn_equals) str = calculate(str);
        txt.setText(str);
    }

    private String calculate(String str) {
        nou = true;
        List<String> str_nums = new ArrayList<>();
        List<Character> symbols = new ArrayList<>();
        List<Character> chars = new ArrayList<>(Arrays.asList('+', '-', '*', '/'));
        str_nums.add("");
        for (int i = 0; i < str.length(); i++) {
            if (chars.contains(str.charAt(i))) {
                symbols.add(str.charAt(i));
                str_nums.add("");
            } else {
                str_nums.set(str_nums.size() - 1, str_nums.get(str_nums.size() - 1) + str.charAt(i));
            }
        }
        if (symbols.size() > 1) return "idk lol";
        if (str_nums.size() < 1) return str_nums.get(0);
        if (str_nums.size() > 2) return "error";
        List<Integer> int_nums = new ArrayList<>();
        for (String s : str_nums) {
            int_nums.add(Integer.parseInt(s));
        }
        String result = "";
        switch (symbols.get(0)) {
            case '+':
                result = (int_nums.get(0) + int_nums.get(1)) + "";
                break;
            case '-':
                result = (int_nums.get(0) - int_nums.get(1)) + "";
                break;
            case '*':
                result = (int_nums.get(0) * int_nums.get(1)) + "";
                break;
            case '/':
                if (int_nums.get(1) == 0) {
                    result = "casi crack";
                } else {
                    result = (int_nums.get(0) / int_nums.get(1)) + "";
                }
                break;
            default:
                break;
        }
        return result;
    }
}