package com.example.formapptest;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layoutnou2);
        TextView result = (TextView) findViewById(R.id.txt_result);
        Button submit = (Button) findViewById(R.id.btn_submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkInputs()) {
                    result.setText(R.string.ok);
                    result.setTextColor(getResources().getColor(R.color.green));
                } else {
                    result.setText(R.string.ko);
                    result.setTextColor(getResources().getColor(R.color.red));
                }
            }
        });

    }
    public boolean checkInputs() {
        String name = ((EditText)findViewById(R.id.inp_name)).getText().toString();
        String surname = ((EditText)findViewById(R.id.inp_surname)).getText().toString();
        String email = ((EditText)findViewById(R.id.inp_email)).getText().toString();
        String birth = ((EditText)findViewById(R.id.inp_birthdate)).getText().toString();

        if (name.isEmpty()) return false;
        if (surname.isEmpty()) return false;
        if (email.isEmpty()) return false;
        if (birth.isEmpty()) return false;

        return true;
    }
}